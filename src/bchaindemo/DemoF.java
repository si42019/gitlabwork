package bchaindemo;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DemoF {

	public static void main(String[] args) throws IOException {
		Path path = Paths.get("/etc","passwd");
		Files.readAllLines(path) // List<String>
		     .stream()           //Stream<String>
		     .parallel()
		     .map(str -> str.substring(0, str.indexOf(":")))
		     .filter(str -> str.contains("a"))
		     .map(String::toUpperCase)
		     .filter(str -> str.length() > 5)
		     .flatMap(str -> str.chars().mapToObj((ch -> ch + "") ))
		     .forEach(System.out::println);
	}
	
}


